﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    private int bulletLeft = 310;
    private int bulletPerMag = 31;
    private int currentBullets;
    private float fireRate = 0.1f;
    private float range = 100f;
    float fireTimer;
    public Transform barrelEnd;
    public Transform shootpoint;
    public ParticleSystem etincele;
    private AudioSource audioSource;
    private AudioSource _audioSource;
    public AudioClip shootSound;
    public AudioClip deadSound;
    public GameObject hitParticule;


    // Start is called before the first frame update
    void Start()
    {
        currentBullets = bulletPerMag;
        audioSource = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            if(currentBullets > 0)
            {
                Fire();
            }
            // Je fais un rayon a partir de BarrelEnd
            Ray bulletRay = new Ray(barrelEnd.position, barrelEnd.forward);

            RaycastHit hit;


            // Si le rayon impacte sur un object, on le propulse
            if (Physics.Raycast(bulletRay, out hit))
            {
                // Ragdoll?
                Ragdoll ragdoll = hit.collider.GetComponentInParent<Ragdoll>();
                if (ragdoll != null)
                {
                    ragdoll.die = true;
                    Debug.Log("L'ennemie a ete touche");
                    PlayDeadSound();
                }
            }
            
        }

        if(fireTimer < fireRate)
        {
            fireTimer += Time.deltaTime; 
        }

        
    }

    private void Fire()
    {
        if (fireTimer < fireRate || currentBullets <= 0)
            return;

        RaycastHit hit;

        if (Physics.Raycast(shootpoint.position, shootpoint.transform.forward, out hit, range))
        {
            GameObject hitParticules = Instantiate(hitParticule, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
            Destroy(hitParticules, 5f);
        }

        if (!Input.GetKey(KeyCode.LeftShift))
        {
            etincele.Play();
            PlayShootSound();
        }

        fireTimer = 0.0f;
    }

    private void PlayShootSound()
    {
        audioSource.PlayOneShot(shootSound);
    }

    private void PlayDeadSound()
    {
        audioSource.PlayOneShot(deadSound);
    }

    //void Explode(Vector3 impactPosition)
    //{
    //    // Recuperer tous les objects qui sont dans le permiterer de l'explosion
    //    Collider[] colliders = Physics.OverlapSphere(impactPosition, 5f);

    //    // Pour chaque object recupere, je lui donne une velocite
    //    foreach (Collider coll in colliders)
    //    {
    //        Rigidbody rb = coll.GetComponent<Rigidbody>();

    //        //Si le rigidbody a ete trouve
    //        if (rb != null)
    //        {
    //            rb.AddExplosionForce(2f, impactPosition, 5f, 0f, ForceMode.Impulse);
    //        }
    //    }
    //}
}
